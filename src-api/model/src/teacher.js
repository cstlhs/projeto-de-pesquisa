const mongoose = require('mongoose')
const validators = require('./validators.js')
var Schema = mongoose.Schema

module.exports = {
  name: 'Teacher',
  definition: {
    workTimeArrangements: Number,
    academicDegree: Number
  },
  options: {
    strict: false,
    collection: 'teacher'
  }
}