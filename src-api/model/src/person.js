const mongoose = require('mongoose')
const validators = require('.validators.js')
var Schema = mongoose.Schema

module.exports = {
  name: 'Person',
  definition: {
    identity: String,
    stateEmission: String,
    dateEmission: { type: Date, default: Date.now },
    cpf: String,
    email: String,
    commercialPhone: String,
    celphoneNumber: String,


  },
  options: {
    strict: false,
    collection: 'person'
  }
}