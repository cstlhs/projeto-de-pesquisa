const mongoose = require('mongoose')
const validators = require('.validators.js')
var Schema = mongoose.Schema

module.exports = {
  name: 'ExpenseElements',
  definition: {
    name: String,
    description: String
  },
  options: {
    strict: false,
    collection: 'expenseElements'
  }
}