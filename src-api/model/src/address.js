const mongoose = require('mongoose')
const validators = require('.validators.js')
var Schema = mongoose.Schema

module.exports = {
  name: 'Address',
  definition: {
    residence: String,
    district: String,
    city: String,
    state: String,
    zipCode: String,
    residencePhone: String
  },
  options: {
    strict: false,
    collection: 'address'
  }
}