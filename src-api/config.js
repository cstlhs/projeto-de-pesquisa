function getApiConfig () {
  return {
    // Stage
    current_stage: process.env.current_stage || 'development',

    // Mongo
    mongoDB_URL: process.env.MONGODB_URI || ('mongodb://localhost:27017/projeto_pesquisa_dev'),

    HOSTNAME: process.env.HOSTNAME || 'http://localhost:3000/',
    ROOT: process.env.ROOT || '/'
  }
}

module.exports = {
  getApiConfig
}
