module.exports = {
  mode: 'universal',
  head: {
    title: 'Projeto de Pesquisa',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Gerador de projetos de pesquisa para a Universidade Católica de Santa Catarina' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons' }
    ]
  },
  plugins: [
    '@/plugins/vuetify'
  ],
  css: [
    '~/assets/style/app.styl'
  ],
  srcDir: './src-nuxt/',
  loading: { color: '#fff' },
  build: {
    // You can extend webpack config here
    extend(config, ctx) {
      // Run ESLint on save
      if (ctx.isDev && ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  },
  modules: [
    '@nuxtjs/axios'
  ],
  proxy: {
    '/api/': process.env.API_URL_BROWSER || 'http://localhost:3000/api/'
  },
  axios: {
    prefix: '/api',
    credentials: true,
    proxy: true
  },
}
