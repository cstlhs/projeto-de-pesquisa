const express = require('express')
const cors = require('cors')
const bodyParser = require('body-parser')
const cookieParser = require('cookie-parser')
const httpsRedirect = require('express-https-redirect')
const compression = require('compression')
const HOST = process.env.HOST || 'localhost'
const PORT = process.env.PORT || 3000
const app = express()

app.use(cors())
app.use(compression())
app.set('trust proxy', 1)
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({
  extended: true
}))
app.use(cookieParser())
if (process.env.NODE_ENV === 'production') {
  app.use('/', httpsRedirect())
}

// Load modules
const MODULES = [
  require('./src-api'),
  require('./src-nuxt')
]

MODULES.forEach(m => m.initialize(app))

app.listen(PORT, HOST)
console.log(`Server listening on ${HOST}:${PORT}`)
